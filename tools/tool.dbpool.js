const Pool = require('pg-pool');

/**
 * @description
 * Class used to launch query to database synchronously or not
 */
class DbPool{

    /**
     * @param {Object} dto
     * @param {string} dto.dbName - the database Name
     * @param {string} dto.dbUser - the database user
     * @param {string} dto.dbHost - the database host
     * @param {string} dto.dbPort - the database port
     * @param {string} dto.dbPassword - the database password
     * @param {number=} [dto.idleTimeoutMillis=60000] - the millisecond timeout before closing idle clients after 60000 second
     * @param {number=} [dto.connectionTimeoutMillis=60000] - the millisecond timeout before returning an error after 60000 second if connection could not be established
     */
    constructor(dto){
        this.pool = new Pool({
            user: dto.dbUser,
            database: dto.dbName,
            host: dto.dbHost,
            password: dto.dbPassword,
            port: dto.dbPort,
            idleTimeoutMillis: 60000,
            connectionTimeoutMillis: 60000,
        });
    }

    //region TIMEOUT SETTINGS
    /**
     * @description
     * Used to change default value of 1min before closing idle clients after #millisecond
     *
     * @param {number} millisecond - the millisecond timeout before closing idle clients after #millisecond
     *
     * @returns {DbPool}
     */
    maxClientTimeout(millisecond){

        if(typeof millisecond !== "number"){
            throw new Error(`
            ===================================
            The max timeout has to be an number
            ===================================
            `);
        }

        this.pool.options.idleTimeoutMillis = millisecond;

        return this;
    }

    /**
     * @description
     * Used to change default value of 1 min before returning an error if connection could not be established
     *
     * @param {number} millisecond - return an error after #millisecond second if connection could not be established
     *
     * @returns {DbPool}
     */
    maxConnectionTimeout(millisecond){

        if(typeof millisecond !== "number"){
            throw new Error(`
            ===================================
            The max timeout has to be an number
            ===================================
            `);
        }

        this.pool.options.connectionTimeoutMillis = millisecond;

        return this;
    }
    //endregionS

    //region EXECUTING FUNCTIONS
    /**
     * @description This allows to launch asynchronously query to the database.
     *
     * @param {string} query - the sql query to launch
     *
     * @return {Promise} - the promise result
     */
    launchAsynchQuery(query){

        return new Promise( (resolve, reject)=>{

            this.pool.connect()
                .then(Client =>{

                    Client.query(query)
                        .then(data =>{

                            Client.release();
                            resolve(data);
                        })
                        .catch(reject);
                })
                .catch(reject);
        });
    }

    /**
     * @description This allows to launch synchronously query to the database.
     *
     * @param {string} query - the sql query to launch
     *
     * @return {Promise} - the awaited "promise" result (data or error)
     */
    async launchSyncQuery(query){
        let error = `Error occurs while connecting or querying database`;
        let client;
        let data;

        try{

            client = await this.pool.connect();
            data = await client.query(query);

            return data;

        } catch(exception){

            console.error(`${error} with ecxeption: \n${exception}`);
            return error;

        } finally {

            if(client) client.release();
        }
    }
    //endregion
}

exports.dbpool = DbPool;